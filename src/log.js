const winston = require('winston');
const dateFormat = require('dateformat');
const _ = require('lodash');

var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      timestamp: function() {
        return Date.now();
      },
      formatter: function(options) {
        var time = dateFormat(options.timestamp(), "mmm dd yyyy HH:MM:ss");
        var level = options.level.toUpperCase();
        var message = options.message;
        var threadId = options.meta && options.meta.threadId;
        var metaToPrint = _.omit(options.meta,'threadId');
        var meta = Object.keys(metaToPrint).length ? JSON.stringify(metaToPrint) : '';
        return `${time} ${level} [T${threadId}] ${message} ${meta}`;
      }
    })
  ]
});

module.exports = logger;
