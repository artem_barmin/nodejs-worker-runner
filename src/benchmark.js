"use strict";

const rabbitMq = require('rabbitmq-wrapper');
const _ = require('lodash');
const uuid = require('uuid');

let queue = new rabbitMq();

function publishTask(task)
{
  queue.publishToQueue('sample-queue', _.merge(task, {requestId: uuid.v4()}), true);
}

// create 10Kb buffer
let sampleBuffer = [];
for (let i = 0;i<10 * 1024; i++)
{
  sampleBuffer.push(i);
}

for (let i = 0;i<100;i++)
{
  publishTask({field: 'callApi', payload: sampleBuffer});
}
publishTask({field: 'callApi', payload: []});
