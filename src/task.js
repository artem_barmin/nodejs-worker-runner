const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/node-tasks');

var taskSchema = mongoose.Schema({
  _id: String,
  status: String,
  threadId: String,
  progress: String,
  updated: { type: Date, default: Date.now }
});

taskSchema.methods.changeStatus = function (status) {
  this.status = status;
  this.save();
}

taskSchema.methods.setProgress = function (progress) {
  this.progress = progress;
  this.save();
}

module.exports = mongoose.model('Task', taskSchema);
