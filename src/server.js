"use strict";

const rabbitMq = require('rabbitmq-wrapper');
const Threads = require('webworker-threads');
const path = require('path');
const log = require('./log');
const fs = require('fs');
const customFunctions = require('./utils/custom-functions');
const co = require('co');
const compileModule = require('./utils/compiler');
const WinstonContext = require('winston-context');
const web = require('./web');
const Task = require('./task');

let queue = new rabbitMq();

// require('./benchmark');

queue.getQueue('sample-queue')
.then((queue) => {
	queue.subscribe((message, headers, deliveryInfo, messageObject) => {
		co(function*()
		{
			let thread = Threads.create();
			let task = new Task({ _id: message.requestId , threadId: thread.id});
			let requestLog = new WinstonContext(log, '', {
				threadId: thread.id
			});

			task.changeStatus('initializing');

			// process requests from thread internals
			thread.on('error', (error) => {
				log.error('In thread', error);
				task.changeStatus('error');
			});
			thread.on('close', (error) => {
				task.changeStatus(error ? 'error' : 'completed');
			});

			let fun = "./src/scripts/" + message.field + "/" + message.field + ".js";
			let funBody = yield compileModule(path.resolve(fun));
			let customFunctionsInternal = fs.readFileSync(path.resolve("./src/utils/custom-functions.js")).toString()
			let threadBody = `
			"use strict";
			var module = {};
			${customFunctionsInternal};
			thread.on('next', function(data) {
				(function()
				{
					(function(thread, data, console)
					{
						${funBody};
					})(null, JSON.parse(data), {log: log, error: log});
				})();
			});
			`;

			// setup all processors for utils
			for (let processorName in customFunctions)
			{
				customFunctions[processorName](thread, requestLog, task);
			}

			requestLog.info('Evaluating:', message.requestId, message.field);
			thread.eval(threadBody);
			task.changeStatus('initialized');
			thread.emit('next', JSON.stringify(message));
			task.changeStatus('processing');

			messageObject.acknowledge();
		});
	});
})
