const express = require('express');
const app = express();
const swig  = require('swig');
const Task = require('./task');
const _ = require('lodash');
const dateFormat = require('dateformat');

function processTask(task)
{
  task.updatedString = dateFormat(task.updated, "mmm dd yyyy HH:MM:ss");
  return task;
}

app.get('/', function (req, res) {
  Task.find({}, null, {sort: {updated: -1}},function(err, tasks) {
    res.send(swig.renderFile('./tasks.html', {tasks: _.map(tasks, processTask)}));
    });
  });

  app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
  });
