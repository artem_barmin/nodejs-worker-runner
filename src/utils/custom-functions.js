module.exports = {};

var requestInProgress = false;

function callHttp(data)
{
  if (requestInProgress)
  {
    throw new Error("Only one http request can be sent at a time. Use 'yeld' to wait");
  }
  requestInProgress = true;
  return new Promise(function(resolve, reject)
  {
    thread.once('httpResponse', (result) =>
    {
      requestInProgress = false;
      resolve(result);
    });
    thread.emit('httpRequest', JSON.stringify(data));
  });
}

function setupHttpProcessor(thread, log)
{
  thread.on('httpRequest', (data) =>
  {
    log.info('Received http request', data);
    setTimeout(() => thread.emit('httpResponse', data), 2000);
  });
}

module.exports.setupHttpProcessor = setupHttpProcessor;

function closeThread()
{
  thread.emit('close');
}

function closeThreadWithError(error)
{
  var error = {stack: error.stack, message: error.message};
  thread.emit('close', JSON.stringify(error));
}

function setupCloseProcessor(thread, log)
{
  thread.on('close', (error) => {
    thread.destroy()
    if (error)
    {
      var error = JSON.parse(error);
      log.error('Thread got error', error.message, error.stack);
    }
    else
    {
      log.info('Thread is closed', thread.id);
    }
  });
}

module.exports.setupCloseProcessor = setupCloseProcessor;

function log()
{
  var args = [].slice.call(arguments);
  thread.emit('log', JSON.stringify(args));
}

function setupLogProcessor(thread, log)
{
  thread.on('log', (data) =>
  {
    log.info.apply(this, JSON.parse(data));
  });
}

module.exports.setupLogProcessor = setupLogProcessor;

function setProgress(progress)
{
  thread.emit('progress', progress);
}

function setupProgressProcessor(thread, log, task)
{
  thread.on('progress', (progress) =>
  {
    task.setProgress(progress);
  });
}

module.exports.setupProgressProcessor = setupProgressProcessor;
