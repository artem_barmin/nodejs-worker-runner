const MemoryFS = require("memory-fs");
const webpack = require("webpack");

function checkSyntax(code)
{
  try {
    new Function(code);
    return true;
  } catch(syntaxError) {
    console.error('Syntax error for function', syntaxError);
    return false;
  }
}

function compileModule(source)
{
  return new Promise((resolve, reject) =>
  {
    var memFs = new MemoryFS();
    var compiler = webpack({
      entry: source,
      output: {
        path: "/",
        filename: "output.js",
      }
    });
    compiler.outputFileSystem = memFs;
    compiler.run(function(err, stats) {
      var fileContent = memFs.readFileSync("/output.js");
      resolve(fileContent.toString());
    });
  });
}

module.exports = compileModule;
