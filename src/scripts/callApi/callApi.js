var _ = require('lodash');
var co = require('co');

log('Start processing inside callApi');

co(function* () {
  if (!data.payload.length)
  {
    throw new Error();
  }
  log('Inside thread');
  var chunks = _.chunk(data.payload, 250);
  for(let i = 0;i<chunks.length;i++)
  {
    let sum = _.sum(chunks[i]);
    console.log('computed sum:', sum); // NOTE: console.log captured by logger
    yield callHttp(sum); // NOTE : kind of sync execution
    setProgress(Math.round(100 * (i / chunks.length)));
  }
}).then(function (value) {
  closeThread();
}, function (err) {
  closeThreadWithError(err);
});
